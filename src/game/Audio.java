package game;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Audio {
    private Clip clip;

    public Audio(String song) {

        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource(song));
            clip = AudioSystem.getClip();
            clip.open(audio);
        } catch (Exception e) {
        }
    }

    public void play() { clip.start(); }

    public static void playSound(String song) {
        Audio s = new Audio(song);
        s.play();
    }
}
