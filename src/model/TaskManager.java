package model;

import gameComponents.Mario;
import gameComponents.Mushroom;
import gameComponents.Turtle;
import controller.Keyboard;
import gameComponents.GameObject;
import gameComponents.Piece;
import view.CreateView;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by BettaMasi on 13/03/17.
 */
public class TaskManager extends Thread{

    public final static int LENGHT_MAP_LEVEL = 4600;

    private final static int POSITION_X_MARIO = 300;
    private final static int POSITION_Y_MARIO = 245;
    private final static int POSITION_X_MUSHROOM = 800;
    private final static int POSITION_Y_MUSHROOM = 263;
    private final static int POSITION_X_TURTLE = 950;
    private final static int POSITION_Y_TURTLE = 243;

    private CreateView gameView;
    private JFrame gameFrame;
    private Mario mario;
    private Mushroom mushroom;
    private Turtle turtle;

    private CreateGameObjects newGameObject = new CreateGameObjects(this);
    private List<GameObject> obstacle = new ArrayList<>();
    private List<Piece> pieces = new ArrayList<>();

    private int xPositionScreen;
    private int marioMovement;

    public TaskManager(JFrame frame){

        this.gameFrame = frame;
        gameView = new CreateView(this);
        gameFrame.add(this.gameView);

        gameFrame.addKeyListener(new Keyboard(this, this.gameView));

        mario = new Mario(POSITION_X_MARIO, POSITION_Y_MARIO, this);
        mushroom = new Mushroom(POSITION_X_MUSHROOM, POSITION_Y_MUSHROOM, this);
        turtle = new Turtle(POSITION_X_TURTLE, POSITION_Y_TURTLE, this);

        obstacle.addAll(newGameObject.getOstacleTunnel());
        obstacle.addAll(newGameObject.getObstacleBlock());
        pieces.addAll(newGameObject.getObstaclePiece());

        this.xPositionScreen = -1;
        this.marioMovement = 0;

        this.gameFrame.setVisible(true);

    }

    @Override
    public void run() {
        while (true) {
            gameView.repaint();

            this.checkCollisionAgainstObstacle();
            this.checkCollisionBetweenCharacters();
            this.checkCollisionWithPieces();

            this.moveObjects();

            try {
                int SLEEP = 3;
                Thread.sleep(SLEEP);
            } catch (InterruptedException e) {
            }
        }
    }

    private void checkCollisionAgainstObstacle() {
        for (int i = 0; i < obstacle.size(); i++) {
            if (this.mario.isNearby(this.obstacle.get(i)))
                this.mario.contact(this.obstacle.get(i));

            if (this.mushroom.isNearby(this.obstacle.get(i)))
                this.mushroom.contact(this.obstacle.get(i));

            if (this.turtle.isNearby(this.obstacle.get(i)))
                this.turtle.contact(this.obstacle.get(i));
        }
    }

    private void checkCollisionBetweenCharacters(){
        if (this.mushroom.isNearby(turtle)) {
            this.mushroom.contact(turtle);
        }
        if (this.turtle.isNearby(mushroom)) {
            this.turtle.contact(mushroom);
        }
        if (this.mario.isNearby(mushroom)) {
            this.mario.contact(mushroom);
            this.mario.setCaracterAlive(true);
            JOptionPane.showMessageDialog(this.gameFrame, "Game over!");
            System.exit(0);
        }
        if (this.mario.isNearby(turtle)) {
            this.mario.contact(turtle);
        }
    }

    private void checkCollisionWithPieces(){
        for (int i = 0; i < this.pieces.size(); i++) {
            if (this.mario.contactPiece(this.pieces.get(i))) {
                this.gameView.playSoundPieces();
                this.pieces.remove(i);
            }
        }
    }

    private void moveObjects()
    {
        if (this.xPositionScreen >= 0 && this.xPositionScreen <= LENGHT_MAP_LEVEL) {
            for (GameObject anObstacle : obstacle) {
                anObstacle.move();
            }

            for (Piece piece : pieces) {
                piece.move();
            }
        }
    }

    public List<GameObject> getObstacle(){
        return Collections.unmodifiableList(this.obstacle);
    }
    public List<Piece> getPieces(){
        return Collections.unmodifiableList(this.pieces);
    }

    public int getxPositionScreen(){
        return this.xPositionScreen;
    }
    public void setxPositionScreen(){ this.xPositionScreen += this.marioMovement; }
    public void setxPositionScreen(int newxPositionScreen){ this.xPositionScreen = newxPositionScreen;};

    public int getMarioMovement(){ return this.marioMovement; }
    public void setMarioMovement(int newMovement){this.marioMovement = newMovement;}

    public Mario getMario(){
        return this.mario;
    }
    public Mushroom getMushroom(){
        return this.mushroom;
    }
    public Turtle getTurtle(){ return this.turtle; }


}
