package gameComponents;

import java.awt.Image;

import model.TaskManager;
import utils.Res;
import utils.Utils;

public class Mario extends GameCharacter {

    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;
    private static final int JUMPING_LIMIT = 42;

    private boolean jumping;
    private int jumpingExtent;

    public Mario(int xPositionOfMario, int yPositionOfMario, TaskManager tmanager) {
        super(xPositionOfMario, yPositionOfMario, WIDTH, HEIGHT, tmanager);
        Image imgMario = Utils.getImage(Res.IMG_MARIO_DEFAULT);
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public boolean isJumping() { return jumping; }
    public void setJumping(boolean isjumping) { this.jumping = isjumping; }

    public Image doJump() {
        String str;

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getyPositionObject() > super.getHeightLimit())
                this.setyPositionObject(this.getyPositionObject() - 4);
            else this.jumpingExtent = JUMPING_LIMIT;

            str = this.getCaracterIsToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if (this.getyPositionObject() + this.getHeightObject() < super.getFloorOffsetY()) {
            this.setyPositionObject(this.getyPositionObject() + 1);
            str = this.getCaracterIsToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            str = this.getCaracterIsToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return Utils.getImage(str);
    }

    public void contact(GameObject gameObject) {
        if (this.hitAhead(gameObject) && this.getCaracterIsToRight() || this.hitBack(gameObject) && !this.getCaracterIsToRight()) {
            super.getTmanager().setMarioMovement(0);
            this.setCaracterMoving(false);
        }

        if (this.hitBelow(gameObject) && this.jumping) {
            super.setFloorOffsetY(gameObject.getyPositionObject());
        } else if (!this.hitBelow(gameObject)) {
            super.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                this.setyPositionObject(MARIO_OFFSET_Y_INITIAL);
            }

            if (hitAbove(gameObject)) {
                this.setHeightLimit(gameObject.getyPositionObject() + gameObject.getHeightObject());
            } else if (!this.hitAbove(gameObject) && !this.jumping) {
                super.setHeightLimit(0);
            }
        }
    }

    public boolean contactPiece(Piece piece) {
        return this.hitBack(piece) || this.hitAbove(piece) || this.hitAhead(piece)
                || this.hitBelow(piece);

    }

    public void contact(GameCharacter character) {
        if (this.hitAhead(character) || this.hitBack(character)) {
            if (character.getCaracterIsAlive()) {
                this.setCaracterMoving(false);
                this.setCaracterAlive(false);
            } else this.setCaracterAlive(true);

        } else if (this.hitBelow(character)) {
            character.setCaracterMoving(false);
            character.setCaracterAlive(false);
        }
    }
}
