package gameComponents;

import java.awt.Image;

import model.TaskManager;
import utils.Res;
import utils.Utils;

public class GameCharacter extends GameObject implements IGameCharacter{

    private static final int PROXIMITY_MARGIN = 10;

    private boolean moving;
    private boolean toRight;
    private int counter;
    private boolean alive;

    private int floorOffsetY;
    private int heightLimit;

    public GameCharacter(int xPositionNewCharacter, int yPositionNewCharacter, int widthNewCharacter, int heightNewCharacter, TaskManager tmanager) {
        super(xPositionNewCharacter, yPositionNewCharacter, widthNewCharacter, heightNewCharacter, tmanager);
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }

    public Image walk(String name, int frequency) {
        return Utils.getImage(Res.IMG_BASE + name + (!this.moving || ++this.counter % frequency == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (this.toRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT);
    }

    public boolean hitAhead(GameObject gameObject) {
        return !(super.getxPositionObject() + super.getWidthObject() < gameObject.getxPositionObject() || super.getxPositionObject() + super.getWidthObject() > gameObject.getxPositionObject() + 5 ||
                super.getyPositionObject() + super.getHeightObject() <= gameObject.getyPositionObject() || super.getyPositionObject() >= gameObject.getyPositionObject() + gameObject.getHeightObject());
    }

    public boolean hitBack(GameObject gameObject) {
        return !(super.getxPositionObject() > gameObject.getxPositionObject() + gameObject.getWidthObject() || super.getxPositionObject() + super.getWidthObject() < gameObject.getxPositionObject() + gameObject.getWidthObject() - 5 ||
                super.getyPositionObject() + super.getHeightObject() <= gameObject.getyPositionObject() || super.getyPositionObject() >= gameObject.getyPositionObject() + gameObject.getHeightObject());
    }

    public boolean hitBelow(GameObject gameObject) {
        return !(super.getxPositionObject() + super.getWidthObject() < gameObject.getxPositionObject() + 5 || super.getxPositionObject() > gameObject.getxPositionObject() + gameObject.getWidthObject() - 5 ||
                super.getyPositionObject() + super.getHeightObject() < gameObject.getyPositionObject() || super.getyPositionObject() + super.getHeightObject() > gameObject.getyPositionObject() + 5);
    }

    public boolean hitAbove(GameObject gameObject) {
        return !(super.getxPositionObject() + super.getWidthObject() < gameObject.getxPositionObject() + 5 || super.getxPositionObject() > gameObject.getxPositionObject() + gameObject.getWidthObject() - 5 ||
                super.getyPositionObject() < gameObject.getyPositionObject() + gameObject.getHeightObject() || super.getyPositionObject() > gameObject.getyPositionObject() + gameObject.getHeightObject() + 5);
    }

    public boolean hitAhead(GameCharacter character) {
        return this.getCaracterIsToRight() && !(super.getxPositionObject() + super.getWidthObject() < character.getxPositionObject() || super.getxPositionObject() + super.getWidthObject() > character.getxPositionObject() + 5 || super.getyPositionObject() + super.getHeightObject() <= character.getyPositionObject() || super.getyPositionObject() >= character.getyPositionObject() + character.getHeightObject());
    }

    public boolean hitBack(GameCharacter character) {
        return !(super.getxPositionObject() > character.getxPositionObject() + character.getWidthObject() || super.getxPositionObject() + super.getWidthObject() < character.getxPositionObject() + character.getWidthObject() - 5 ||
                super.getyPositionObject() + super.getHeightObject() <= character.getyPositionObject() || super.getyPositionObject() >= character.getyPositionObject() + character.getHeightObject());
    }

    public boolean hitBelow(GameCharacter character) {
        return !(super.getxPositionObject() + super.getWidthObject() < character.getxPositionObject() || super.getxPositionObject() > character.getxPositionObject() + character.getWidthObject() ||
                super.getyPositionObject() + super.getHeightObject() < character.getyPositionObject() || super.getyPositionObject() + super.getHeightObject() > character.getyPositionObject());
    }

    public boolean isNearby(GameCharacter character) {
        return (super.getxPositionObject() > character.getxPositionObject() - PROXIMITY_MARGIN && super.getxPositionObject() < character.getxPositionObject() + character.getWidthObject() + PROXIMITY_MARGIN)
                || (super.getxPositionObject() + super.getWidthObject() > character.getxPositionObject() - PROXIMITY_MARGIN && super.getxPositionObject() + super.getWidthObject() < character.getxPositionObject() + character.getWidthObject() + PROXIMITY_MARGIN);
    }

    public boolean isNearby(GameObject gameObject) {
        return (super.getxPositionObject() > gameObject.getxPositionObject() - PROXIMITY_MARGIN && super.getxPositionObject() < gameObject.getxPositionObject() + gameObject.getWidthObject() + PROXIMITY_MARGIN) ||
                (super.getxPositionObject() + super.getWidthObject() > gameObject.getxPositionObject() - PROXIMITY_MARGIN && super.getxPositionObject() + super.getWidthObject() < gameObject.getxPositionObject() + gameObject.getWidthObject() + PROXIMITY_MARGIN);
    }

    public boolean getCaracterIsAlive() {
        return alive;
    }
    public void setCaracterAlive(boolean alive) {
        this.alive = alive;
    }

    public boolean getCaracterIsToRight() {
        return toRight;
    }
    public void setCaracterToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public void setCaracterMoving(boolean moving) {
        this.moving = moving;
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }
    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public int getHeightLimit() { return heightLimit;}
    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }
}
