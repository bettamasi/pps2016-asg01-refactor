package gameComponents;

import model.TaskManager;
import utils.Res;
import utils.Utils;

public class Tunnel extends GameObject {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 65;

    public Tunnel(int xPositionNewTunnel, int yPositionNewTunnel, TaskManager tmanager) {
        super(xPositionNewTunnel, yPositionNewTunnel, WIDTH, HEIGHT, tmanager);
        super.setImgObj(Utils.getImage(Res.IMG_TUNNEL));
    }

}
