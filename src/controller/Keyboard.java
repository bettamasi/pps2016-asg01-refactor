package controller;

import game.Audio;
import model.TaskManager;
import view.CreateView;

import java.awt.event.*;

public class Keyboard implements KeyListener {

    private TaskManager tmanager;
    private CreateView createView;

    public Keyboard(TaskManager tmanager, CreateView createView){
        this.tmanager = tmanager;
        this.createView = createView;
    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (tmanager.getMario().getCaracterIsAlive()) {
            switch (e.getKeyCode()){
                case KeyEvent.VK_RIGHT:
                    if (this.tmanager.getxPositionScreen() == -1) {
                        this.tmanager.setxPositionScreen(0);
                        this.createView.setXpositionBackgroundUp(-50);
                        this.createView.setXpositionBackgroundUnder(750);
                    }
                    this.tmanager.getMario().setCaracterMoving(true);
                    this.tmanager.getMario().setCaracterToRight(true);
                    this.tmanager.setMarioMovement(1); // si muove verso sinistra
                    break;

                case KeyEvent.VK_LEFT:
                    if (this.tmanager.getxPositionScreen() == 4601) {
                        this.tmanager.setxPositionScreen(4600);
                        this.createView.setXpositionBackgroundUp(-50);
                        this.createView.setXpositionBackgroundUnder(750);
                    }

                    this.tmanager.getMario().setCaracterMoving(true);
                    this.tmanager.getMario().setCaracterToRight(false);
                    this.tmanager.setMarioMovement(-1); // si muove verso destra
                    break;

                case KeyEvent.VK_UP:
                    this.tmanager.getMario().setJumping(true);
                    Audio.playSound("/resources/audio/jump.wav");
                    break;

            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        this.tmanager.getMario().setCaracterMoving(false);
        this.tmanager.setMarioMovement(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
