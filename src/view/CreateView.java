package view;

import game.Audio;
import model.TaskManager;
import utils.Res;
import utils.Utils;

import javax.swing.*;
import java.awt.*;



/**
 * Created by BettaMasi on 13/03/17.
 */
public class CreateView extends JPanel {

    private static final int X_POSITION_FLAG = 4650;
    private static final int Y_POSITION_FLAG = 115;
    private static final int X_POSITION_CASTLE = 4850;
    private static final int Y_POSITION_CASTLE = 145;
    private static final int FREQUENCY_OF_MARIO = 25;
    private static final int FREQUENCY_OF_MUSHROOM = 45;
    private static final int FREQUENCY_TURTLE = 45;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;

    private Image imgBackground1, imgBackground2, firstCastle, imgStart, finishFlag, finishCastle;

    private int xpositionBackgroundUp, xpositionBackgroundUnder;

    private TaskManager tmanager;


    public CreateView(TaskManager tmanager){
        super();

        this.tmanager = tmanager;

        this.xpositionBackgroundUp = -50;
        this.xpositionBackgroundUnder = 750;

        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.firstCastle = Utils.getImage(Res.IMG_CASTLE);
        this.imgStart = Utils.getImage(Res.START_ICON);

        this.finishCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.finishFlag = Utils.getImage(Res.IMG_FLAG);

    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        this.updateBackgroundOnMovement();

        g.drawImage(this.imgBackground1, this.xpositionBackgroundUp, 0, null);
        g.drawImage(this.imgBackground2, this.xpositionBackgroundUnder, 0, null);
        g.drawImage(this.firstCastle, 10 - this.tmanager.getxPositionScreen(), 95, null);
        g.drawImage(this.imgStart, 220 - this.tmanager.getxPositionScreen(), 234, null);

        for (int i = 0; i < this.tmanager.getObstacle().size(); i++) {
            g.drawImage(this.tmanager.getObstacle().get(i).getImgObj(), this.tmanager.getObstacle().get(i).getxPositionObject(),
                    this.tmanager.getObstacle().get(i).getyPositionObject(), null);
        }

        for (int i = 0; i < this.tmanager.getPieces().size(); i++) {
            g.drawImage(this.tmanager.getPieces().get(i).imageOnMovement(), this.tmanager.getPieces().get(i).getxPositionObject(),
                    this.tmanager.getPieces().get(i).getyPositionObject(), null);
        }

        g.drawImage(this.finishFlag, this.X_POSITION_FLAG - this.tmanager.getxPositionScreen(), this.Y_POSITION_FLAG, null);
        g.drawImage(this.finishCastle, this.X_POSITION_CASTLE - this.tmanager.getxPositionScreen(), this.Y_POSITION_CASTLE, null);

        if (this.tmanager.getMario().isJumping()) {
            g.drawImage(this.tmanager.getMario().doJump(), this.tmanager.getMario().getxPositionObject(), this.tmanager.getMario().getyPositionObject(), null);

        }else {
            g.drawImage(this.tmanager.getMario().walk(Res.IMGP_CHARACTER_MARIO, this.FREQUENCY_OF_MARIO), this.tmanager.getMario().getxPositionObject(), this.tmanager.getMario().getyPositionObject(), null);
        }

        if (this.tmanager.getMushroom().getCaracterIsAlive())
            g.drawImage(this.tmanager.getMushroom().walk(Res.IMGP_CHARACTER_MUSHROOM, this.FREQUENCY_OF_MUSHROOM), this.tmanager.getMushroom().getxPositionObject(), this.tmanager.getMushroom().getyPositionObject(), null);
        else
            g.drawImage(this.tmanager.getMushroom().deadImage(), this.tmanager.getMushroom().getxPositionObject(), this.tmanager.getMushroom().getyPositionObject() + this.MUSHROOM_DEAD_OFFSET_Y, null);


        if (this.tmanager.getTurtle().getCaracterIsAlive())
            g.drawImage(this.tmanager.getTurtle().walk(Res.IMGP_CHARACTER_TURTLE, this.FREQUENCY_TURTLE), this.tmanager.getTurtle().getxPositionObject(), this.tmanager.getTurtle().getyPositionObject(), null);
        else
            g.drawImage(this.tmanager.getTurtle().deadImage(), this.tmanager.getTurtle().getxPositionObject(), this.tmanager.getTurtle().getyPositionObject() + this.TURTLE_DEAD_OFFSET_Y, null);

    }

    private void updateBackgroundOnMovement() {
        if (this.tmanager.getxPositionScreen() >= 0 && this.tmanager.getxPositionScreen() <= this.tmanager.LENGHT_MAP_LEVEL) {
            this.tmanager.setxPositionScreen();

            // Moving the screen to give the impression that Mario is walking
            this.xpositionBackgroundUp = this.xpositionBackgroundUp - this.tmanager.getMarioMovement();
            this.xpositionBackgroundUnder = this.xpositionBackgroundUnder - this.tmanager.getMarioMovement();
        }
        this.checkBackgroundPosition();
    }

    private void checkBackgroundPosition(){
        // Flipping between background1 and background2
        if (this.xpositionBackgroundUp == -800) {
            this.xpositionBackgroundUp= 800;
        }else if (this.xpositionBackgroundUnder == -800) {
            this.xpositionBackgroundUnder = 800;
        }else if (this.xpositionBackgroundUp == 800) {
            this.xpositionBackgroundUp = -800;
        }else if (this.xpositionBackgroundUnder == 800) {
            this.xpositionBackgroundUnder = -800;
        }
    }

    public void playSoundPieces(){
        Audio.playSound(Res.AUDIO_MONEY);
    }

    public void setXpositionBackgroundUp(int newXPositionBackgroundUp){ this.xpositionBackgroundUp = newXPositionBackgroundUp; }
    public void setXpositionBackgroundUnder(int newXPositionBackgroundUnder){ this.xpositionBackgroundUnder = newXPositionBackgroundUnder; }
}
